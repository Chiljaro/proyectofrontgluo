import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import MovesDays from './components/MovesDays.vue'
import AdminUsers from './components/AdminUsers.vue'
import ProfileGuest from './components/ProfileGuest.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/inicio',
      name: 'home',
      component: Home
    },
    {
      path: '/movesDays',
      name: 'movesDays',
      component: MovesDays
    },
    {
      path: '/adminUsers',
      name: 'adminUser',
      component: AdminUsers
    },
    {
      path: '/profilGeuest',
      name: 'profileGuest',
      component: ProfileGuest
    },
  ]
})
