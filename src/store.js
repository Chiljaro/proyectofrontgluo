import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    promociones: [
      {
        promo: 'Gluo Rewards', 
        subjet: '¡Gran venta de verano!', 
        description: 'Hasta 20% de descuento en hoteles que cuenten con salas de juntas incluídas, ingresando el código:', 
        photo: './assets/promo/banner.png', 
        code: 'REWARDS-VERANO',
        identificador: 'promo1', 
      },
      {
        promo: 'Otra promo', 
        subjet: '¡Verano!', 
        description: 'Hasta 20% de descuento en hoteles que cuenten con salas de juntas incluídas, ingresando el código:', 
        photo: './assets/promo/banner.png', 
        code: 'REWARDS-VERANO',
        identificador: 'promo2', 
      },
    ]

  },
  mutations: {

  },
  actions: {

  }
})
